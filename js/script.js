window.addEventListener("load", init, false);

// Variables globales
step = -1;
codeLines = [];
mainVariables = [];
functionVariables = [];

mode = 0; // animacion completa

function init() {
    var run, pause, forward;
    
    run = document.getElementById("run");
    run.addEventListener("click", runAnimation, false);
    
    pause = document.getElementById("pause");
    pause.addEventListener("click", pauseAnimation, false);
    
    forward = document.getElementById("forward");
    forward.addEventListener("click", forwardAnimation, false);
    
    /*********************************************************************
       Obtención de objetos estáticos (no cambian en todo la ejecución 
    **********************************************************************/
    
    /////////////////////////
    // Variables principal //
    /////////////////////////
    var mainVariablesList = document.getElementsByClassName("variables-principal");
    mainVariables = Array.prototype.slice.call(mainVariablesList[0].children, 0);
    
    ///////////////////////
    // Variables funcion //
    ///////////////////////
    var functionVariablesList = document.getElementsByClassName("variables-funcion");
    functionVariables = Array.prototype.slice.call(functionVariablesList[0].children, 0);
    
    objMainFunction = document.getElementsByClassName("funcion-principal")[0];
    objAddFunction = document.getElementsByClassName("funcion-suma")[0];
     
    reset();
}

/************************************************************
 Reinicia valores y animaciones 
************************************************************/
function reset()
{ 
    /////////////////////////////////
    // Animación opacidad. Reseteo //
    /////////////////////////////////
    
    objAddFunction.className = objAddFunction.className.replace( /(?:^|\s)desactiva-funcion(?!\S)/g , '' );
    objAddFunction.style.animationPlayState = "";
    objMainFunction.className = objMainFunction.className.replace( /(?:^|\s)desactiva-funcion(?!\S)/g , '' );
    objMainFunction.style.animationPlayState = "";

    // ¡¡ OJO!!! para reiniciar la animación necesito eliminar el nodo y volver a insertarlo
    var newObjAddFunction = objAddFunction.cloneNode(true);
    objAddFunction.parentNode.replaceChild(newObjAddFunction, objAddFunction);
    objAddFunction = newObjAddFunction; 

    var newObjMainFunction = objMainFunction.cloneNode(true);
    objMainFunction.parentNode.replaceChild(newObjMainFunction, objMainFunction);
    objMainFunction = newObjMainFunction; 
    
    /////////////////////////////////
    // Lineas de código a ejecutar //
    /////////////////////////////////
    var executableLinesBlocks = document.getElementsByClassName("codigo-a-ejecutar");
   
    // convierto las instrucciones principales de lista a array
    codeLines = Array.prototype.slice.call(executableLinesBlocks[0].children, 0);
    // inserto las instruccione ejecutables de la función en el orden del proceso
    codeLines.splice.apply(codeLines, [4,0].concat(Array.prototype.slice.call(executableLinesBlocks[1].children)));
    
    ///////////////
    // Listeners //
    ///////////////
    
    addEventListeners();
    
     objMainFunction.addEventListener("animationiteration", function() {
        objMainFunction.style.animationPlayState = "paused";
    }, false);

    objAddFunction.addEventListener("animationiteration", function() {
        objAddFunction.style.animationPlayState = "paused";
    }, false);
 
    /////////////////////////////////////////////////////
    // Reinicio texto y animacions de líneas de código //
    /////////////////////////////////////////////////////
    
    for (cont = 0, len = codeLines.length; cont < len; cont++) {
        codeLines[cont].style.animationPlayState = "running";
        codeLines[cont].className = codeLines[cont].className.replace( /(?:^|\s)ejecucion-parcial(?!\S)/g , '' );
        codeLines[cont].className = codeLines[cont].className.replace( /(?:^|\s)ejecucion(?!\S)/g , '' );
    }
    
    for (cont = 0, len = mainVariables.length; cont < len; cont++) {
        mainVariables[cont].children[0].innerHTML = "0";
    }
    
    for (cont = 0, len = functionVariables.length; cont < len; cont++) {
        functionVariables[cont].children[0].innerHTML = "0";
    }
    
}

/************************************************************
 Arranca la animación
************************************************************/
function runAnimation() {
    
    // reinicio valores
    reset(); 
    
    step = 0;
    
    document.getElementById("pause").innerHTML = "Pausa";

    codeLines[0].className += " ejecucion";
    objAddFunction.className += " desactiva-funcion";
}

/************************************************************
 Pausa la animación
************************************************************/
function pauseAnimation() {
    
    var pausedBoton = document.getElementById("pause");
    
    if ((codeLines[step].style.animationPlayState == "running"
       || !codeLines[step].style.animationPlayState) && mode == 0)
    { 
        codeLines[step].style.animationPlayState = "paused";
        pausedBoton.innerHTML = "Continuar";
    }
    else // continuar
    {
        addEventListeners();
        
        codeLines[step].style.animationPlayState = "running";
        pausedBoton.innerHTML = "Pausa";
        if (mode == 1)
        {
            codeLines[step].className = codeLines[step].className.replace( /(?:^|\s)ejecucion-parcial(?!\S)/g , '' );
            codeLines[step].className = codeLines[step].className.replace( /(?:^|\s)ejecucion(?!\S)/g , '' );
            step++;
            manageCases();
            codeLines[step].className += " ejecucion";
            mode = 0;
        }
    }
}

/************************************************************
 Animación paso a paso
************************************************************/
function forwardAnimation() {
    mode = 1; // animacion paso a paso
    
    var pausedBoton = document.getElementById("pause");
    pausedBoton.innerHTML = "Continuar";

    removeEventListeners();
    
    // en el caso de que estuviera una pausada
    if (step > -1)
    {
       codeLines[step].style.animationPlayState = "running";
       codeLines[step].className = codeLines[step].className.replace( /(?:^|\s)ejecucion-parcial(?!\S)/g , '' );
       codeLines[step].className = codeLines[step].className.replace( /(?:^|\s)ejecucion(?!\S)/g , '' );
    }
    
    if (step == 7) return;
    
    step++;
    
    codeLines[step].className+=" ejecucion-parcial";
    
    manageCases();
}

/************************************************************
 Enlaza las animaciones
 ************************************************************/
function manageAnimation() 
{
    if (step == 7) return;
    
    step++;
    
    // elimino la clase ejecución del paso anterior mediante una expresion regular
    codeLines[step-1].className = codeLines[step-1].className.replace( /(?:^|\s)ejecucion(?!\S)/g , '' ); 
        
    codeLines[step].className += " ejecucion";
      
    manageCases();
 }

/************************************************************
 Realiza tratamientos especificos para cada paso
 ************************************************************/
function manageCases()
{
    switch (step) 
    {
        case 0: 
            objAddFunction.className += " desactiva-funcion";  // para el modo paso a paso
            break;
        case 2:
            mainVariables[0].children[0].innerHTML = "3";
            break;
        case 3:
            mainVariables[1].children[0].innerHTML = "5";
            break;
        case 4:
            objAddFunction.style.animationPlayState = "running";
            objMainFunction.className += " desactiva-funcion";
            objMainFunction.style.animationPlayState = "running";
            
            functionVariables[0].children[0].innerHTML = "3";
            functionVariables[1].children[0].innerHTML = "5";
            break;
        case 6:
            functionVariables[2].children[0].innerHTML = "8";
            break;
        case 7:
            objAddFunction.style.animationPlayState = "running";
            objMainFunction.style.animationPlayState = "running";
            
            mainVariables[2].children[0].innerHTML = "8";
            break;
            
    }
}

/************************************************************
 Asocia a cada uno de las lineas de código un event listener 
 para detectar el fin de la animación
 ************************************************************/
function addEventListeners() 
{
    // asocio evento fin de animacion
    for (cont = 0, len = codeLines.length; cont < len; cont++) {
        codeLines[cont].addEventListener("animationend",manageAnimation,false);
    }
}


/************************************************************
 Elimino los listeners asociados a cada uno de las lineas de 
 código usados para detectar el fin de la animación
 ************************************************************/
function removeEventListeners() 
{
    // elimino la asociacion de los eventos
    for (cont = 0, len = codeLines.length; cont < len; cont++) {
        codeLines[cont].removeEventListener("animationend",manageAnimation);    
    }
}
